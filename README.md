A simple installer script for ubuntu and os x to install the splunk forwarder software, relies on a downloaded .deb or darwin .tgz file. Test a fair bit on ubuntu, some on OS X. It can also install a splunk application .zip file.


Usage:  [OPTIONS]   

   -v = Echo the version number of this script

   -i = Set the indexer

   -d = Set path to Darwin .tgz file

   -p = Set path to splunkforwarder .deb file

   -b = Name of a splunk application zip file (but this needs in the dir with the script)

   -u = Uninstall in a brutal way