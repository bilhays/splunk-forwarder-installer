#!/bin/bash
# copyright 2016 bil hays
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>
    
# Version .9
# -Added os x support
# Version .8
# -Expanded getopts
# -added uninstall
# -More error checks
# -added auto start
# Version .5, starting point


version="0.8";

PREFIX="/opt/splunkforwarder";
INPUTS="${PREFIX}/etc/system/local/inputs.conf"
SPLUNK="${PREFIX}/bin/splunk";
SPLUNKAPPS="${PREFIX}/etc/apps/";
INDEXER="";
UNZIP=$(which unzip);
UNINSTALL=0;
#####
# Functions


# Tells the user how the program expects to run
# Returns zero on success
# No input arguments
function usage
   {
   echo "";
   echo "Usage: ${PROGRAM} [OPTIONS]" >&2;
   echo "   -v = Echo the version number of this script" >&2;
   echo "   -i = set the indexer" >&2;
   echo "   -d = set path to Darwin .tgz file" >&2;
   echo "   -p = Set path to splunkforwarder .deb file" >&2;
   echo "   -b = path to a splunk application zip file" >&2;
   echo "   -u = Uninstall" >&2;
   echo;
   exit;
   }

function idos
   {
   if [[ $(uname) == "Darwin" ]]
   then 
      # We're on OSX
      echo "Darwin";
      #exit 0;
      # Check to see if we're on linux system
   elif  [[ $(uname) == "Linux" ]]
      then 
      # We're on linux
      if [[ $(cat /etc/issue | grep "Red Hat") ]]
         then 
         echo "Redhat";
         #exit 0;
      elif [[ $(cat /etc/issue | grep "Ubuntu") ]]
         then
         echo "Ubuntu";
         #exit 0;
      fi      
   fi
   }

function check_errm
   {
   if  [[ ${?} != "0" ]]
      then
      echo "${1}";
      exit ${2};
      fi  
   }

if [[ ! ${1} ]]
   then
   usage;
   exit;
fi

function uninstall()
   {
   ${SPLUNK} stop;
   echo "Uninstalling splunk forwarder";
   if [[ -f /etc/init.d/splunk ]]
      then
      
      rm "/etc/init.d/splunk";
   fi
   rm -fr "/opt/splunkforwarder";
   if [[ -f "/Library/LaunchDaemons/com.splunk.plist" ]]
      then
      launchctl stop com.splunk;
      launchctl unload "/Library/LaunchDaemons/com.splunk.plist";
      rm "/Library/LaunchDaemons/com.splunk.plist";
   fi
   }

while getopts "vi:d:p:b:u" Option
do
  case $Option in
    v     ) echo;
            echo "   Version is ${version}";
    		echo;
            exit;;
    i     ) echo "   Using ${OPTARG} as indexer";
    		INDEXER=${OPTARG};;
    d     ) echo "   Path to darwin tgz file is ${OPTARG}";
          DARPATH=${OPTARG};;	    		
    p     ) echo "   Path to deb file is ${OPTARG}";
          DEBPATH=${OPTARG};;	
	u     ) echo "   Uninstalling...";
	        UNINSTALL="1";;
	b     ) echo;
	        echo "   Path to application is ${OPTARG}";
	        SPLUNKDEPLOY=${OPTARG};;
    *     ) 
          echo "Unimplemented option chosen.";
          usage;
          ;;   # DEFAULT
   esac
done
check_errm "getopts failed in some way" "1";

if [[ ${UNINSTALL} == 1 ]]
   then
   uninstall   
   exit 0;
fi   

OS=$(idos);
echo "We are $OS";
   
# install unzip if it is not
if [[ ! -f "${UNZIP}" ]]
   then 
   if [[ ${OS} == "Darwin" ]] 
      then
      check_errm "   ***No unzip found, dying here" "9";
      exit 6;
   else
      apt-get install unzip;   
      check_errm "   ***Install of unzip failed" "10";
   fi
else
   echo "We have unzip, carry on;"
fi 

# ID OS for forking installer 

if [[ ${OS} == "Ubuntu" ]]
   then
   dpkg -i "${DEBPATH}";
   check_errm "Install of ${DEBPATH} failed" "20";
elif [[  ${OS} == "Darwin" ]]
   then 
   echo "Installing darwin tarball";
   # check for /opt, if it's not there make it
   if [[ ! -d "/opt" ]]
      then
      echo "mkdir /opt";
      mkdir "/opt"
   fi
   # check to see if DARPATH is in this dir, if it's not, copy it in.
   if [[ "${DARPATH}" != ${DARPATH##/*/} ]]
      then
      echo "${DARPATH} not in local dir";
      cp "${DARPATH}" "./";
   else 
      echo "${DARPATH} in local dir";         
   fi   
   # unpack it and push it to /opt
   tar -xzvf "${DARPATH}" ;
   check_errm "tar extract of ${DARPATH} failed" "22";
   rsync -av ./splunkforwarder/* /opt/splunkforwarder/
fi  

# start and accept the license
${SPLUNK} start --accept-license --answer-yes --auto-ports --no-prompt
check_errm "${SPLUNK} start --accept-license failed" "30";

# Check the conf file for the index, if we have a match don't 
# add it.
if [[ ! $(grep "${INDEXER}" "${INPUTS}") ]]
   then
   echo "Index apparently not set, setting....";
   echo >> "${INPUTS}";
   echo "   index = ${INDEXER}"  >> "${INPUTS}";
   check_errm "Failed to set index to ${INDEXER} in inputs.conf" "40";
fi

# by default, log all *.log files in /var/log
if [[ ! $(grep "/var/log/" "${INPUTS}") ]]
   then
   echo "/var/log path not in inputs, setting...";
   echo "   [monitor:///var/log/*.log]" >> "${INPUTS}";
   check_errm "Failed to set [monitor:///var/log/*.log] in inputs.conf" "50";
fi

echo "Installing ${SPLUNKDEPLOY} to ${SPLUNKAPPS}";
cp "${SPLUNKDEPLOY}" "${SPLUNKAPPS}";
check_errm "copy of ${SPLUNKDEPLOY} failed" "60";

pushd "${SPLUNKAPPS}"
if [[ -d ${SPLUNKDEPLOY%.*} ]]
   then 
   echo "${SPLUNKDEPLOY%.*} apparently installed, carry on";
else
   ${UNZIP} ${SPLUNKDEPLOY}
   check_errm "unzip ${SPLUNKDEPLOY} failed" "70";
   rm  ${SPLUNKDEPLOY};
fi
popd;


# By default, splunk installs the launchd file as an agent
# Agents only run if someone logs in, and we don't like that.
# So we install the plist as a LaunchDaemon
if [[ ${OS} == "Darwin" ]] 
   then
   echo "copying com.splunk.plist to /Library/LaunchDaemons/";
   cp "./com.splunk.plist"   "/Library/LaunchDaemons/com.splunk.plist";
   check_errm "copy of com.splunk.plist failed" "75";
   launchctl load /Library/LaunchDaemons/com.splunk.plist;
   check_errm "launchctl load /Library/LaunchDaemons/com.splunk.plist" "76";
else
   ${SPLUNK} enable boot-start;
   check_errm "Enable boot-start failed" "80"
fi
	
${SPLUNK} restart;
check_errm "${SPLUNK} restart failed" "90";



